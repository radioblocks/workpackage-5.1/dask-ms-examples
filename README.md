# dask-ms examples

## Setup overview
In order to run the dask-ms examples presented in Jupyter Notebooks below, you need to setup a Python environment with the following packages installed:

    - dask
    - dask_labextension
    - jupyter
    - xarray
    - dask-ms

## Launching
On a typical HPC system, you will first require some computational resources, launch Jupyter lab and connect from an external web browser.

On a HPC system using Slurm for its job scheduler, the sequence would look like, e.g.:

```
$ salloc -t 01:00:00 --cpus-per-task 20 --mem 80G --partition <partition-name> --qos <qos-name>

$ srun --pty bash
```

At this stage you sit on the allocated compute node where you can interactively launch JupyterLab, here manually setting the port to 8888:

```
ipnport=8888
jupyter-lab --no-browser --port=${ipnport} --ip=$(hostname -i)
```

You will need to establish SSH port forwarding for tunneling the Jupyter Lab port (here set to 8888) and Dask dashboard port (default is 8787) from the compute node previously allocated to your local machine.

Then, from in your local web browser, copy paste the URL provided by JupyterLab which looks like (including the token):

```
http://127.0.0.1:8888/lab?token=7fbde52aad1d993e78a9f7bd28915a40ed71fe6f1dab9bb6
```

If everything is configured properly you should see the Dask dashboard.

## AOFlagger on Dask
Two notebooks are provided to run Python AOFlagger on Dask xarray datasets prepared by dask-ms:

    - dask-ms_aoflagger_bsl.ipynb
    - dask-ms_aoflagger.ipynb

``dask-ms_aoflagger_bsl.ipynb`` is based on an intuitive but inefficient use of ``dask-ms`` that generates xarray dataset for each baseline. As each dataset is backed up by deferred calls to casacore MS reading functions this appears to be pretty inefficient. A better approach is taken in ``dask-ms_aoflagger_bsl.ipynb``, where each dataset contains all baselines for a given antenna. Then AOFlagger is applied locally on each baseline, but the number of calls to casacore is hence driven by the number of stations, and not the number of baselines.

For a MWA dataset with 124 stations, 28 integration times and 768 frequency channels, using 20 physical CPU cores of a single node of EPFL GPU cluster Izar (Intel(R) Xeon(R) Gold 6230 CPU @ 2.10GHz), we have:


|                 | dask-ms_aoflagger_bsl | dask-ms_aoflagger |
|:----------------|---------------------: | -----------------:|
| Dask cluster    |                 2.3 s |             4.1 s |
| xds_from_ms     |               145.4 s |             3.6 s |
| dataset length  |                  7750 |               124 |
| Dask futures    |                24.8 s |             0.5 s |
| Processing time |               178.2 s |            25.9 s |
| Fraction of time reading data |  85.8 % |            75.4 % |
| **Total time**  |           **350.7 s** |        **34.1 s** |

That is, by simply tuning we form xarray datasets in dask-ms we can improve compute times by a factor of 10.

### **Disclaimer**
We opened the following issue with AOFlagger: https://gitlab.com/aroffringa/aoflagger/-/issues/40, with respect to using existing flags in the MS before running AOFlagger. Hence we cannot always (depending on the datasets) match the results of AOFlagger when run from the command line. Hence we cannot provide a one to one comparison at this stage.

## Preprocessing visibility data for BIPP
Another application of dask-ms is presented under the Jupyter Notebook ``dask-ms_bipp_vis.ipynb`` in which Dask is used to parallelize the data ingestion for BIPP (https://arxiv.org/abs/2310.09200) which represent a significant part of the overall runtime. To run this example, you'll have to install BIPP, see instructions here: https://github.com/epfl-radio-astro/bipp.

**TODO: provide access to MS datasets**

**TODO: provide slides comparing Dask vs multiprocessing**




